# Fábrica de Garrafas Plimm 🍾

![Plimm Bottle](https://example.com/plimm_bottle_image.jpg)

Bem-vindo à Fábrica de Garrafas Plimm! Este projeto é dedicado à geração de dados virtuais de garrafas de rum para o seu entretenimento. 🎉

## Visão Geral

O projeto `plimm_bottle` é um programa simples em C que gera dados virtuais de garrafas de rum com vários atributos, como marca, modelo, rótulo, ano, produtor e origem. Ele usa UUIDs para identificação única, criando um conjunto de dados rico em garrafas de rum.

## Como Começar

Para começar com a Fábrica de Garrafas Plimm, siga estas etapas:

1. Clone o repositório:

    ```bash
    git clone https://gitlab.com/scoobiii/plimm_bottle.git
    cd plimm_bottle
    ```

2. Compile e execute o programa:

    ```bash
    gcc -o plimm_bottle plimm_bottle.c -luuid
    ./plimm_bottle
    ```

3. Aproveite os dados gerados de garrafas de rum!

## Recursos

- Gera dados virtuais de garrafas de rum.
- Utiliza UUIDs para identificação única.
- Atribui aleatoriamente características como marca, modelo, rótulo, ano, produtor e origem.

## Visualização

Sinta-se à vontade para visualizar os dados gerados de garrafas de rum usando suas ferramentas preferidas ou integrar recursos adicionais ao projeto.

## Contribuição

Aceitamos contribuições! Se você tiver ideias ou melhorias, crie uma solicitação de merge ou abra uma issue.

## Licença

Este projeto é licenciado sob a [Licença MIT](LICENSE).

## Status do Projeto

O desenvolvimento na Fábrica de Garrafas Plimm está ativo e em andamento. Se você tiver alguma dúvida ou sugestão, por favor, nos avise.

Aproveite a criação de garrafas de rum virtuais com a Fábrica de Garrafas Plimm! 🍾✨